#include <gtest/gtest.h>
#include "../train.h"

using namespace train_test_task;

TEST(constructor, group) {
  int length = std::rand() % 10000;
  Train train = Train(length);
  size_t light_on_cnt = 0;
  size_t light_off_cnt = 0;
  auto data = train.get_data();

  for (auto elem : data) {
    if (elem == true) {
      light_on_cnt++;
    } else {
      light_off_cnt++;
    }
  }
  double light_on_probability =
      static_cast<double>(light_on_cnt) / static_cast<double>(length);
  double light_off_probability =
      static_cast<double>(light_off_cnt) / static_cast<double>(length);

  ASSERT_NE(light_on_cnt, 0);
  ASSERT_NE(light_off_cnt, 0);
  ASSERT_NEAR(light_on_probability, light_off_probability, 0.1);
}

// TEST(spawn, group) {
//   int length = std::rand() % 100;
//   Train train = Train(length);
//   for (uint32_t i = 0; i < 1000; i++) {
//     ASSERT_EQ(train.spawn(), i);
//   }
// }

TEST(light_manipulation, group) {
  int length = std::rand() % 100;
  Train train = Train(length);
  train.spawn();
  train.turn_off_light();
  ASSERT_EQ(train.get_light(), false);
  train.turn_on_light();
  ASSERT_EQ(train.get_light(), true);
}

TEST(traveler_moves, group) {
  int length = std::rand() % 100;
  Train train = Train(length);
  train.spawn();
  const auto carriage_number_before = train.get_carriage();
  train.next_carriage();
  const auto carriage_number_next = train.get_carriage();
  train.prev_carriage();
  const auto carriage_number_prev = train.get_carriage();

  ASSERT_EQ(carriage_number_before, carriage_number_prev);
  ASSERT_EQ((carriage_number_before + 1) % length, carriage_number_next);
}

TEST(traveler_backwards_movement, group) {
  int length = std::rand() % 100;
  Train train = Train(length);
  train.spawn();

  for (uint32_t i = 0; i < length; i++) {
    train.turn_off_light();
    train.prev_carriage();
  }
  auto data = train.get_data();

  for (auto elem : data) {
    if (elem == true) {
      FAIL();
    }
  }
}

TEST(traveler_forward_movement, group) {
  int length = std::rand() % 100;
  Train train = Train(length);
  train.spawn();

  for (uint32_t i = 0; i < length; i++) {
    train.turn_off_light();
    train.next_carriage();
  }
  auto data = train.get_data();

  for (auto elem : data) {
    if (elem == true) {
      FAIL();
    }
  }
}

TEST(traveler_forward_backwards_movement, group) {
  int length = std::rand() % 100;
  Train train = Train(length);
  train.spawn();

  for (uint32_t i = 0; i < length; i++) {
    train.turn_off_light();
    train.next_carriage();
  }
  for (uint32_t i = 0; i < length; i++) {
    bool light = train.get_light();
    if (light == true) {
      FAIL();
    }
    train.next_carriage();
  }
}

TEST(begin_right_alg, group) {
  for (uint32_t i = 0; i < 100; i++) {
    int length = std::rand() % 1000;
    Train train = Train(length);
    ASSERT_EQ(begin_right_alg(train), length);
  }
}

// TEST(right_left_alg, group)
// {
//     for (uint32_t i = 0; i < 100; i++)
//     {
//         int length = std::rand() % 100;
//         Train train = Train(length);
//         std::cout << length << std::endl;
//         ASSERT_EQ(right_left_alg(train), length);
//     }

// }