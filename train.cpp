#include "train.h"
#include <algorithm>
#include <iostream>

using namespace train_test_task;

// #define PRINT

Train::Train() : Train(std::rand() % 100) {}

Train::Train(size_t length) : length_(length) {
  data_.resize(length_);
  for (auto elem : data_) {
    elem = std::rand() % 2 == 0;
  }
}

void Train::spawn() { carriage_ = std::rand() % length_; }

bool Train::get_light() const { return data_[carriage_]; }

void Train::turn_off_light() { data_[carriage_] = false; }

void Train::turn_on_light() { data_[carriage_] = true; }

void Train::next_carriage() { carriage_ = (carriage_ + 1) % length_; }

void Train::prev_carriage() { carriage_ = (carriage_ + length_ - 1) % length_; }
void Train::print_state() {
#ifdef PRINT
  for (std::size_t i = 0, e = data_.size(); i != e; ++i) {
    if (i == carriage_) {
      std::cout << "(" << data_[i] << ")" << ' ';
    } else {
      std::cout << " " << data_[i] << " " << ' ';
    }
  }
  std::cout << std::endl;
#endif
}

size_t train_test_task::begin_right_alg(Train train) {
  auto go_right_until_light_on = [](Train &train) {
    size_t length = 0;
    do {
      if (length != 0) {
        train.turn_off_light();
      }
      train.next_carriage();
      length++;
    } while (train.get_light() != true);
    return length;
  };

  auto return_left_n = [](Train &train, size_t length) {
    for (size_t i = 0; i < length; i++) {
      train.prev_carriage();
    }
  };

  train.spawn();
  train.turn_on_light();
  train.print_state();
  size_t length = 0;
  do {
    length = go_right_until_light_on(train);
    train.turn_off_light();
    return_left_n(train, length);
  } while (train.get_light() == true);
  return length;
}

// doesn't work
size_t train_test_task::right_left_alg(Train train) {
  train.spawn();
  train.turn_on_light();
  train.print_state();
  size_t length_right = 0;
  size_t length_left = 0;

  auto go_right_until_light_on = [](Train &train) {
    size_t length = 0;
    do {
      // if (length_right != 0)
      { train.turn_off_light(); }
      train.next_carriage();
      length++;
    } while (train.get_light() != true);
    return length;
  };

  auto go_left_until_light_on = [](Train &train) {
    size_t length = 0;
    do {
      // if (length_right != 0)
      { train.turn_off_light(); }
      train.prev_carriage();
      length++;
    } while (train.get_light() != true);
    return length;
  };

  auto return_left_n = [](Train &train, size_t length) {
    for (size_t i = 0; i < length; i++) {
      train.prev_carriage();
    }
  };

  auto return_right_n = [](Train &train, size_t length) {
    for (size_t i = 0; i < length; i++) {
      train.next_carriage();
    }
  };

  do {
    std::cout << "iteration" << std::endl;
    length_right = go_right_until_light_on(train);
    return_left_n(train, length_right);
    length_left = go_left_until_light_on(train);
    train.turn_off_light();
    return_right_n(train, length_left + length_right);

  } while (train.get_light() == true);

  return length_right + length_left;
}