#ifndef TRAIN_H
#define TRAIN_H

#include <stdint.h>
#include <map>
#include <vector>

namespace train_test_task {

class Train {
 private:
  //  public:
  std::vector<bool> data_;
  size_t length_;
  size_t carriage_;

 public:
  Train();
  explicit Train(size_t length);
  void spawn();
  bool get_light() const;
  void turn_off_light();
  void turn_on_light();
  void next_carriage();
  void prev_carriage();
  const auto get_data() const { return data_; }
  size_t get_carriage() const { return carriage_; }
  void print_state();
};

size_t begin_right_alg(Train train);
// doesn't work
size_t right_left_alg(Train train);

}  // namespace train_test_task

#endif